﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct VirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>
struct AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1;
// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4;
// Google.ProtocolBuffers.ByteString
struct ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A;
// Google.ProtocolBuffers.ExtensionRegistry
struct ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>
struct GeneratedBuilderLite_2_t71624524B8C71DE8E3AFB975ACAF500327EC847C;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A;
// Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>
struct GeneratedMessageLite_2_t870B09013459A3BF308F2237885D0655826C244B;
// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219;
// Google.ProtocolBuffers.ICodedInputStream
struct ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5;
// Google.ProtocolBuffers.ICodedOutputStream
struct ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89;
// Google.ProtocolBuffers.IMessageLite
struct IMessageLite_tBFF4C3D45EE47902CE049D63AE119D373003C2A3;
// Google.ProtocolBuffers.InvalidProtocolBufferException
struct InvalidProtocolBufferException_t33420AA524353DDC202DCBD0462457CC2AA7B4F9;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>
struct Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>>
struct Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_tFF77EB203CF12E843446A71A6581145AB929D681;
// System.Collections.Generic.IComparer`1<System.String>
struct IComparer_1_t1355C31540122E494D24DC775351D9341C5BD17C;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.StringComparer
struct StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011;
// proto.PhoneEvent/Types/OrientationEvent/Builder
struct Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49;

extern RuntimeClass* Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49_il2cpp_TypeInfo_var;
extern RuntimeClass* CodedOutputStream_t6FF414144C567C520A212C17587CA998689956E3_il2cpp_TypeInfo_var;
extern RuntimeClass* ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_il2cpp_TypeInfo_var;
extern RuntimeClass* ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var;
extern RuntimeClass* ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var;
extern RuntimeClass* OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var;
extern RuntimeClass* PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
extern RuntimeClass* StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____FADC743710841EB901D5F6FBC97F555D4BD94310_5_FieldInfo_var;
extern String_t* _stringLiteral11F6AD8EC52A2984ABAAFD7C3B516503785C2072;
extern String_t* _stringLiteral395DF8F7C51F007019CB30201C49E884B46B92FA;
extern String_t* _stringLiteral95CB0BFD2977C761298D9624E4B4D4C72A39974A;
extern String_t* _stringLiteralA5A01B8FA531FAAD566300A7EEDEFD11C4D01FDE;
extern String_t* _stringLiteralAFF024FE4AB0FECE4091DE044C58C9AE4233383A;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_mB19EE5F0FCF8140DD90EC96A03AD672683526FA6_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_mD453C9051F68A00AE78E9EF83B8CF76CA5A95E06_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1B4DB2FF2F658CEC5A805885FFCFA08D5D81CAC4_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m35222F82356FD2422BE24164EDC6125DF9D48154_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m496959C5DABEC8E4AFBD6277B4960F92581DCFAE_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m51A99B2CC2EE65A2B8D33C7DFA55BBB9D3925F3F_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m64B17EDD1282A929E92A080231C411AEB35A6A6D_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m9C44F027BC780DD743CB3B193540B3DF97CD068E_RuntimeMethod_var;
extern const RuntimeMethod* Array_BinarySearch_TisString_t_mBE069358BEE81E8A0072F4E656453331D6AE5596_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m74707E7D8855B78D106754E0AEAFFE0D6F86CC84_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m776B1D0BC795128DB70DC82AA6E45A4D5508F8C7_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m7981F3DA5593744CAB82590CEB30C86C092DC9A4_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2__ctor_m3900653DB821F4DDF90C2DA3A49DC84616BF3475_RuntimeMethod_var;
extern const uint32_t Builder_Clear_mAED01EBE4F83E902B41ECE689E2423C75ECC129B_MetadataUsageId;
extern const uint32_t Builder_Clone_mE746997FB7380030F257D53226BA90972952B836_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m276BB4322A07E03DE7E02DFD374A0C7FB2F85271_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m56553FBB92C57C9D8281DE9E23A8BC24FF4C5A65_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m74707E7D8855B78D106754E0AEAFFE0D6F86CC84_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_mC1EB74CC5F94FDE8331D4FECC8C7E0297E9DD8BD_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E_MetadataUsageId;
extern const uint32_t Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA_MetadataUsageId;
extern const uint32_t Builder__ctor_mF49ADBA40C169B49ADB90170B1065D8DC3D4ED01_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m8C95578C47726F0C0DBB1963A2EAE0781BC2E4B3_MetadataUsageId;
extern const uint32_t OrientationEvent_CreateBuilderForType_m3313C216FC4A71AD4E03370190678EA46C6810CB_MetadataUsageId;
extern const uint32_t OrientationEvent_CreateBuilder_m2EE04D707C94FC05F8365845F50D7FF61AB08011_MetadataUsageId;
extern const uint32_t OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086_MetadataUsageId;
extern const uint32_t OrientationEvent_Equals_m581EB99D43E5E5D2D7B9307DE40FF44B6E52E7B6_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseDelimitedFrom_m67B8A477040C545D71769E23259BF1F2F9321D90_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseDelimitedFrom_mB75C1304397BF83946189648904C7628122A3DCF_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m5C2ACFB25E390D22981AEC8F71776A156E93A1AD_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m73086E4B29C6D7F7FB28C301E49F7D4299B84DED_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m73A5952E28CB199961E941B40540B0AD69C3893D_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_mA4CDBD35E004A50A76B87B7E10F766DA1B2EEB13_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_mB405823EAFF5C1AFD6B6EF122898B280D5729991_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_mDCB8A835A3DC278361CEBCC50576707FEAB3FF0A_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_mE020500B4CD835952E09898B82FDD034F74A4466_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_mE97DBBD2F6C98DEC6DCB5DD5BDAB00B00A15F4A2_MetadataUsageId;
extern const uint32_t OrientationEvent_PrintTo_m35DBBD6CEB205610733D810D83FAC3C370AE149F_MetadataUsageId;
extern const uint32_t OrientationEvent_ToBuilder_mC9B7F27ABEC0AD40B243E77F34170D30B40A1A21_MetadataUsageId;
extern const uint32_t OrientationEvent_WriteTo_m3DAC611BEBCD5363561CE7AE2DE615DE6D752738_MetadataUsageId;
extern const uint32_t OrientationEvent__cctor_mAD299E8068E26BDA6F7A1691D05B02AC25859F9A_MetadataUsageId;
extern const uint32_t OrientationEvent__ctor_mD121E47486ECAC2644B82A490759C5A601F98D9E_MetadataUsageId;
extern const uint32_t OrientationEvent_get_DefaultInstanceForType_m8925260733DE1C5DC508D453ABCA75E899F7EFEA_MetadataUsageId;
extern const uint32_t OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91_MetadataUsageId;
extern const uint32_t OrientationEvent_get_SerializedSize_mA55B8909849B20383F1D72807BF2688A23299D94_MetadataUsageId;
extern const uint32_t PhoneEvent__cctor_m43334645A72D0A443EDF366DB61D7C2A42CA69C1_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTBUILDERLITE_2_T634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4_H
#define ABSTRACTBUILDERLITE_2_T634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4_H
#ifndef ABSTRACTMESSAGELITE_2_TA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5_H
#define ABSTRACTMESSAGELITE_2_TA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  AbstractMessageLite_2_tA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_TA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5_H
#ifndef BYTESTRING_T484699ED4C1CFF8928F9242C75BD5F6532CF768A_H
#define BYTESTRING_T484699ED4C1CFF8928F9242C75BD5F6532CF768A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ByteString
struct  ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A  : public RuntimeObject
{
public:
	// System.Byte[] Google.ProtocolBuffers.ByteString::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_1;

public:
	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A, ___bytes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

struct ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A_StaticFields
{
public:
	// Google.ProtocolBuffers.ByteString Google.ProtocolBuffers.ByteString::empty
	ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A_StaticFields, ___empty_0)); }
	inline ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * get_empty_0() const { return ___empty_0; }
	inline ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESTRING_T484699ED4C1CFF8928F9242C75BD5F6532CF768A_H
#ifndef EXTENSIONREGISTRY_TAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_H
#define EXTENSIONREGISTRY_TAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ExtensionRegistry
struct  ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>> Google.ProtocolBuffers.ExtensionRegistry::extensionsByName
	Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B * ___extensionsByName_1;
	// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite> Google.ProtocolBuffers.ExtensionRegistry::extensionsByNumber
	Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 * ___extensionsByNumber_2;
	// System.Boolean Google.ProtocolBuffers.ExtensionRegistry::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_extensionsByName_1() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E, ___extensionsByName_1)); }
	inline Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B * get_extensionsByName_1() const { return ___extensionsByName_1; }
	inline Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B ** get_address_of_extensionsByName_1() { return &___extensionsByName_1; }
	inline void set_extensionsByName_1(Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B * value)
	{
		___extensionsByName_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByName_1), value);
	}

	inline static int32_t get_offset_of_extensionsByNumber_2() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E, ___extensionsByNumber_2)); }
	inline Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 * get_extensionsByNumber_2() const { return ___extensionsByNumber_2; }
	inline Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 ** get_address_of_extensionsByNumber_2() { return &___extensionsByNumber_2; }
	inline void set_extensionsByNumber_2(Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 * value)
	{
		___extensionsByNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByNumber_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_StaticFields
{
public:
	// Google.ProtocolBuffers.ExtensionRegistry Google.ProtocolBuffers.ExtensionRegistry::empty
	ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_StaticFields, ___empty_0)); }
	inline ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * get_empty_0() const { return ___empty_0; }
	inline ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONREGISTRY_TAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGCOMPARER_T588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_H
#define STRINGCOMPARER_T588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparer
struct  StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE  : public RuntimeObject
{
public:

public:
};

struct StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_StaticFields
{
public:
	// System.StringComparer System.StringComparer::_invariantCulture
	StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * ____invariantCulture_0;
	// System.StringComparer System.StringComparer::_invariantCultureIgnoreCase
	StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * ____invariantCultureIgnoreCase_1;
	// System.StringComparer System.StringComparer::_ordinal
	StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * ____ordinal_2;
	// System.StringComparer System.StringComparer::_ordinalIgnoreCase
	StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * ____ordinalIgnoreCase_3;

public:
	inline static int32_t get_offset_of__invariantCulture_0() { return static_cast<int32_t>(offsetof(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_StaticFields, ____invariantCulture_0)); }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * get__invariantCulture_0() const { return ____invariantCulture_0; }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE ** get_address_of__invariantCulture_0() { return &____invariantCulture_0; }
	inline void set__invariantCulture_0(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * value)
	{
		____invariantCulture_0 = value;
		Il2CppCodeGenWriteBarrier((&____invariantCulture_0), value);
	}

	inline static int32_t get_offset_of__invariantCultureIgnoreCase_1() { return static_cast<int32_t>(offsetof(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_StaticFields, ____invariantCultureIgnoreCase_1)); }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * get__invariantCultureIgnoreCase_1() const { return ____invariantCultureIgnoreCase_1; }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE ** get_address_of__invariantCultureIgnoreCase_1() { return &____invariantCultureIgnoreCase_1; }
	inline void set__invariantCultureIgnoreCase_1(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * value)
	{
		____invariantCultureIgnoreCase_1 = value;
		Il2CppCodeGenWriteBarrier((&____invariantCultureIgnoreCase_1), value);
	}

	inline static int32_t get_offset_of__ordinal_2() { return static_cast<int32_t>(offsetof(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_StaticFields, ____ordinal_2)); }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * get__ordinal_2() const { return ____ordinal_2; }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE ** get_address_of__ordinal_2() { return &____ordinal_2; }
	inline void set__ordinal_2(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * value)
	{
		____ordinal_2 = value;
		Il2CppCodeGenWriteBarrier((&____ordinal_2), value);
	}

	inline static int32_t get_offset_of__ordinalIgnoreCase_3() { return static_cast<int32_t>(offsetof(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_StaticFields, ____ordinalIgnoreCase_3)); }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * get__ordinalIgnoreCase_3() const { return ____ordinalIgnoreCase_3; }
	inline StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE ** get_address_of__ordinalIgnoreCase_3() { return &____ordinalIgnoreCase_3; }
	inline void set__ordinalIgnoreCase_3(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * value)
	{
		____ordinalIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier((&____ordinalIgnoreCase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARER_T588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#define PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.Proto.PhoneEvent
struct  PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072  : public RuntimeObject
{
public:

public:
};

struct PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields
{
public:
	// System.Object proto.Proto.PhoneEvent::Descriptor
	RuntimeObject * ___Descriptor_0;

public:
	inline static int32_t get_offset_of_Descriptor_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields, ___Descriptor_0)); }
	inline RuntimeObject * get_Descriptor_0() const { return ___Descriptor_0; }
	inline RuntimeObject ** get_address_of_Descriptor_0() { return &___Descriptor_0; }
	inline void set_Descriptor_0(RuntimeObject * value)
	{
		___Descriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___Descriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T65235398342E0C6C387633EE4B2E7F326A539C91_H
#define __STATICARRAYINITTYPESIZEU3D12_T65235398342E0C6C387633EE4B2E7F326A539C91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T65235398342E0C6C387633EE4B2E7F326A539C91_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#define __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#define __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_TD2672E72DE7681E58346994836A2016B555BF4C1_H
#define __STATICARRAYINITTYPESIZEU3D28_TD2672E72DE7681E58346994836A2016B555BF4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_TD2672E72DE7681E58346994836A2016B555BF4C1_H
#ifndef GENERATEDBUILDERLITE_2_T5FE12C42B399AD2099894359F40D69FC579B1B9A_H
#define GENERATEDBUILDERLITE_2_T5FE12C42B399AD2099894359F40D69FC579B1B9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A  : public AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T5FE12C42B399AD2099894359F40D69FC579B1B9A_H
#ifndef GENERATEDMESSAGELITE_2_TF07B6BF883A996942E93FD145584FDDE91AC5219_H
#define GENERATEDMESSAGELITE_2_TF07B6BF883A996942E93FD145584FDDE91AC5219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219  : public AbstractMessageLite_2_tA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_TF07B6BF883A996942E93FD145584FDDE91AC5219_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#define BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef TEXTWRITER_T92451D929322093838C41489883D5B2D7ABAF3F0_H
#define TEXTWRITER_T92451D929322093838C41489883D5B2D7ABAF3F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___CoreNewLine_9;
	// System.IFormatProvider System.IO.TextWriter::InternalFormatProvider
	RuntimeObject* ___InternalFormatProvider_10;

public:
	inline static int32_t get_offset_of_CoreNewLine_9() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0, ___CoreNewLine_9)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_CoreNewLine_9() const { return ___CoreNewLine_9; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_CoreNewLine_9() { return &___CoreNewLine_9; }
	inline void set_CoreNewLine_9(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___CoreNewLine_9 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_9), value);
	}

	inline static int32_t get_offset_of_InternalFormatProvider_10() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0, ___InternalFormatProvider_10)); }
	inline RuntimeObject* get_InternalFormatProvider_10() const { return ___InternalFormatProvider_10; }
	inline RuntimeObject** get_address_of_InternalFormatProvider_10() { return &___InternalFormatProvider_10; }
	inline void set_InternalFormatProvider_10(RuntimeObject* value)
	{
		___InternalFormatProvider_10 = value;
		Il2CppCodeGenWriteBarrier((&___InternalFormatProvider_10), value);
	}
};

struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___Null_1;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteCharDelegate_2;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteStringDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteStringDelegate_3;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharArrayRangeDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteCharArrayRangeDelegate_4;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteLineCharDelegate_5;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineStringDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteLineStringDelegate_6;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharArrayRangeDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteLineCharArrayRangeDelegate_7;
	// System.Action`1<System.Object> System.IO.TextWriter::_FlushDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____FlushDelegate_8;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ___Null_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}

	inline static int32_t get_offset_of__WriteCharDelegate_2() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteCharDelegate_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteCharDelegate_2() const { return ____WriteCharDelegate_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteCharDelegate_2() { return &____WriteCharDelegate_2; }
	inline void set__WriteCharDelegate_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteCharDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&____WriteCharDelegate_2), value);
	}

	inline static int32_t get_offset_of__WriteStringDelegate_3() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteStringDelegate_3)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteStringDelegate_3() const { return ____WriteStringDelegate_3; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteStringDelegate_3() { return &____WriteStringDelegate_3; }
	inline void set__WriteStringDelegate_3(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteStringDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&____WriteStringDelegate_3), value);
	}

	inline static int32_t get_offset_of__WriteCharArrayRangeDelegate_4() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteCharArrayRangeDelegate_4)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteCharArrayRangeDelegate_4() const { return ____WriteCharArrayRangeDelegate_4; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteCharArrayRangeDelegate_4() { return &____WriteCharArrayRangeDelegate_4; }
	inline void set__WriteCharArrayRangeDelegate_4(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteCharArrayRangeDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&____WriteCharArrayRangeDelegate_4), value);
	}

	inline static int32_t get_offset_of__WriteLineCharDelegate_5() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteLineCharDelegate_5)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteLineCharDelegate_5() const { return ____WriteLineCharDelegate_5; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteLineCharDelegate_5() { return &____WriteLineCharDelegate_5; }
	inline void set__WriteLineCharDelegate_5(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteLineCharDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&____WriteLineCharDelegate_5), value);
	}

	inline static int32_t get_offset_of__WriteLineStringDelegate_6() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteLineStringDelegate_6)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteLineStringDelegate_6() const { return ____WriteLineStringDelegate_6; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteLineStringDelegate_6() { return &____WriteLineStringDelegate_6; }
	inline void set__WriteLineStringDelegate_6(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteLineStringDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&____WriteLineStringDelegate_6), value);
	}

	inline static int32_t get_offset_of__WriteLineCharArrayRangeDelegate_7() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteLineCharArrayRangeDelegate_7)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteLineCharArrayRangeDelegate_7() const { return ____WriteLineCharArrayRangeDelegate_7; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteLineCharArrayRangeDelegate_7() { return &____WriteLineCharArrayRangeDelegate_7; }
	inline void set__WriteLineCharArrayRangeDelegate_7(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteLineCharArrayRangeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&____WriteLineCharArrayRangeDelegate_7), value);
	}

	inline static int32_t get_offset_of__FlushDelegate_8() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____FlushDelegate_8)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__FlushDelegate_8() const { return ____FlushDelegate_8; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__FlushDelegate_8() { return &____FlushDelegate_8; }
	inline void set__FlushDelegate_8(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____FlushDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&____FlushDelegate_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T92451D929322093838C41489883D5B2D7ABAF3F0_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::16E2B412E9C2B8E31B780DE46254349320CCAAA0
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::25B4B83D2A43393F4E18624598DDA694217A6622
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___25B4B83D2A43393F4E18624598DDA694217A6622_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::311441405B64B3EA9097AC8E07F3274962EC6BB4
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___311441405B64B3EA9097AC8E07F3274962EC6BB4_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::FADC743710841EB901D5F6FBC97F555D4BD94310
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___FADC743710841EB901D5F6FBC97F555D4BD94310_5;

public:
	inline static int32_t get_offset_of_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0() const { return ___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0() { return &___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0; }
	inline void set_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0 = value;
	}

	inline static int32_t get_offset_of_U325B4B83D2A43393F4E18624598DDA694217A6622_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___25B4B83D2A43393F4E18624598DDA694217A6622_1)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U325B4B83D2A43393F4E18624598DDA694217A6622_1() const { return ___25B4B83D2A43393F4E18624598DDA694217A6622_1; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U325B4B83D2A43393F4E18624598DDA694217A6622_1() { return &___25B4B83D2A43393F4E18624598DDA694217A6622_1; }
	inline void set_U325B4B83D2A43393F4E18624598DDA694217A6622_1(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___25B4B83D2A43393F4E18624598DDA694217A6622_1 = value;
	}

	inline static int32_t get_offset_of_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___311441405B64B3EA9097AC8E07F3274962EC6BB4_2)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2() const { return ___311441405B64B3EA9097AC8E07F3274962EC6BB4_2; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2() { return &___311441405B64B3EA9097AC8E07F3274962EC6BB4_2; }
	inline void set_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___311441405B64B3EA9097AC8E07F3274962EC6BB4_2 = value;
	}

	inline static int32_t get_offset_of_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3() const { return ___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3() { return &___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3; }
	inline void set_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3 = value;
	}

	inline static int32_t get_offset_of_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4() const { return ___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4() { return &___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4; }
	inline void set_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4 = value;
	}

	inline static int32_t get_offset_of_FADC743710841EB901D5F6FBC97F555D4BD94310_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___FADC743710841EB901D5F6FBC97F555D4BD94310_5)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_FADC743710841EB901D5F6FBC97F555D4BD94310_5() const { return ___FADC743710841EB901D5F6FBC97F555D4BD94310_5; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_FADC743710841EB901D5F6FBC97F555D4BD94310_5() { return &___FADC743710841EB901D5F6FBC97F555D4BD94310_5; }
	inline void set_FADC743710841EB901D5F6FBC97F555D4BD94310_5(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___FADC743710841EB901D5F6FBC97F555D4BD94310_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#define RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef ORIENTATIONEVENT_TE448386384E4E5CC5C4FFBB5A22055845E0A9011_H
#define ORIENTATIONEVENT_TE448386384E4E5CC5C4FFBB5A22055845E0A9011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent
struct  OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011  : public GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/OrientationEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::z_
	float ___z__14;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasW
	bool ___hasW_16;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::w_
	float ___w__17;
	// System.Int32 proto.PhoneEvent/Types/OrientationEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_18;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_hasW_16() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasW_16)); }
	inline bool get_hasW_16() const { return ___hasW_16; }
	inline bool* get_address_of_hasW_16() { return &___hasW_16; }
	inline void set_hasW_16(bool value)
	{
		___hasW_16 = value;
	}

	inline static int32_t get_offset_of_w__17() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___w__17)); }
	inline float get_w__17() const { return ___w__17; }
	inline float* get_address_of_w__17() { return &___w__17; }
	inline void set_w__17(float value)
	{
		___w__17 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_18() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___memoizedSerializedSize_18)); }
	inline int32_t get_memoizedSerializedSize_18() const { return ___memoizedSerializedSize_18; }
	inline int32_t* get_address_of_memoizedSerializedSize_18() { return &___memoizedSerializedSize_18; }
	inline void set_memoizedSerializedSize_18(int32_t value)
	{
		___memoizedSerializedSize_18 = value;
	}
};

struct OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields
{
public:
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::defaultInstance
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/OrientationEvent::_orientationEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____orientationEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/OrientationEvent::_orientationEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____orientationEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields, ___defaultInstance_0)); }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldNames_1() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields, ____orientationEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__orientationEventFieldNames_1() const { return ____orientationEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__orientationEventFieldNames_1() { return &____orientationEventFieldNames_1; }
	inline void set__orientationEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____orientationEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldTags_2() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields, ____orientationEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__orientationEventFieldTags_2() const { return ____orientationEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__orientationEventFieldTags_2() { return &____orientationEventFieldTags_2; }
	inline void set__orientationEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____orientationEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONEVENT_TE448386384E4E5CC5C4FFBB5A22055845E0A9011_H
#ifndef BUILDER_TCAC10B81FDF15941DE7FAC380E33D08FDF542C49_H
#define BUILDER_TCAC10B81FDF15941DE7FAC380E33D08FDF542C49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent/Builder
struct  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49  : public GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::result
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49, ___result_1)); }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * get_result_1() const { return ___result_1; }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_TCAC10B81FDF15941DE7FAC380E33D08FDF542C49_H
#ifndef TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#define TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/Type
struct  Type_tE66B948E7250AA336E8768E46508619266BABE96 
{
public:
	// System.Int32 proto.PhoneEvent/Types/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tE66B948E7250AA336E8768E46508619266BABE96, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#ifndef INVALIDPROTOCOLBUFFEREXCEPTION_T33420AA524353DDC202DCBD0462457CC2AA7B4F9_H
#define INVALIDPROTOCOLBUFFEREXCEPTION_T33420AA524353DDC202DCBD0462457CC2AA7B4F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.InvalidProtocolBufferException
struct  InvalidProtocolBufferException_t33420AA524353DDC202DCBD0462457CC2AA7B4F9  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPROTOCOLBUFFEREXCEPTION_T33420AA524353DDC202DCBD0462457CC2AA7B4F9_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GeneratedMessageLite_2__ctor_mE63935D3D4D82B2BDDD9B07D9FDE9D10745CD1C8_gshared (GeneratedMessageLite_2_t870B09013459A3BF308F2237885D0655826C244B * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void GeneratedMessageLite_2_PrintField_mC27392F14B8111BF5863427B7451556DD9EFC7EA_gshared (String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * p3, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_mAFD20E7C462DDE6B9FCE7B5817C5CB43CB263CF9_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * p0, const RuntimeMethod* method);
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::BuildParsed()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GeneratedBuilderLite_2_BuildParsed_m0D1289DDF2C1215A97B51EC7B64373017726AE1E_gshared (GeneratedBuilderLite_2_t71624524B8C71DE8E3AFB975ACAF500327EC847C * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m5F8C43F21D84FA150B80D9083CB9BF6BEC663024_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m513DBFC93D76859C6B81B47EC5F546F6174716B2_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m44A37F0234A9CDCAD8C4D09AC7F47932CD9BE893_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_mCBB8D52A39B14EFC5B7ACFB43DDE748383500108_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m81D9306326551B80945DAD0531EE64C07135FB0E_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeDelimitedFrom_m0505B5836359EC961E6065A397E671C33DA77353_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeDelimitedFrom_m8022E9ECE595D0E6CCF5833FF1297912C17087D1_gshared (AbstractBuilderLite_2_t660AC10BDBEF676E29D9A22DE5C4187D16654BB1 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GeneratedBuilderLite_2__ctor_m47ECDA285A179A11564535C61736FEFDE7E55C8D_gshared (GeneratedBuilderLite_2_t71624524B8C71DE8E3AFB975ACAF500327EC847C * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GeneratedBuilderLite_2_MergeFrom_m897D3373027F726F8270C9E4D6D5B08DAC83E648_gshared (GeneratedBuilderLite_2_t71624524B8C71DE8E3AFB975ACAF500327EC847C * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 System.Array::BinarySearch<System.Object>(!!0[],!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR int32_t Array_BinarySearch_TisRuntimeObject_mCA23887A81438487583626C2E739C0C9A3805B3E_gshared (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p0, RuntimeObject * p1, RuntimeObject* p2, const RuntimeMethod* method);

// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::.ctor()
inline void GeneratedMessageLite_2__ctor_m3900653DB821F4DDF90C2DA3A49DC84616BF3475 (GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219 *, const RuntimeMethod*))GeneratedMessageLite_2__ctor_mE63935D3D4D82B2BDDD9B07D9FDE9D10745CD1C8_gshared)(__this, method);
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91 (const RuntimeMethod* method);
// System.Int64 proto.PhoneEvent/Types/OrientationEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t OrientationEvent_get_Timestamp_m27DD78A0CF1A72F2DC6F35D22915A9DC8CEBEAE2 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_X()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_X_m9DC6D26DA8E3997CCCF3AAE2C3645E2EF572586D (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Y()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Y_m6B75F5A2DC77CC5A2725BDFB6BBD3A50337C6E93 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Z()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Z_mFBA154D9C74251325CDB8A828C1A7FCC3292915C (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_W()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_W_m61106CB54F7209E45F4761631647856D6ED39925 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.CodedOutputStream::ComputeInt64Size(System.Int32,System.Int64)
extern "C" IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeInt64Size_m469FA3555088F5F6436873B0B862B27DAC90A566 (int32_t p0, int64_t p1, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.CodedOutputStream::ComputeFloatSize(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeFloatSize_m590AFD0BAD36720168DBFB2EA91754E846F70BC7 (int32_t p0, float p1, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 System.Int64::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Int64_GetHashCode_mB5F9D4E16AFBD7C3932709B38AD8C8BF920CC0A4 (int64_t* __this, const RuntimeMethod* method);
// System.Int32 System.Single::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0 (float* __this, const RuntimeMethod* method);
// System.Boolean System.Int64::Equals(System.Int64)
extern "C" IL2CPP_METHOD_ATTR bool Int64_Equals_mB589D15F558BF8FECBB56EF429EFF5C7A39D9E0F (int64_t* __this, int64_t p0, const RuntimeMethod* method);
// System.Boolean System.Single::Equals(System.Single)
extern "C" IL2CPP_METHOD_ATTR bool Single_Equals_mCDFA927E712FBA83D076864E16C77E39A6E66FE7 (float* __this, float p0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3 (String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * p3, const RuntimeMethod* method)
{
	((  void (*) (String_t*, bool, RuntimeObject *, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_mC27392F14B8111BF5863427B7451556DD9EFC7EA_gshared)(p0, p1, p2, p3, method);
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086 (const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeFrom_m51A99B2CC2EE65A2B8D33C7DFA55BBB9D3925F3F (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * p0, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_mAFD20E7C462DDE6B9FCE7B5817C5CB43CB263CF9_gshared)(__this, p0, method);
}
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::BuildParsed()
inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2 (GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A * __this, const RuntimeMethod* method)
{
	return ((  OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * (*) (GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A *, const RuntimeMethod*))GeneratedBuilderLite_2_BuildParsed_m0D1289DDF2C1215A97B51EC7B64373017726AE1E_gshared)(__this, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeFrom_m9C44F027BC780DD743CB3B193540B3DF97CD068E (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A *, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m5F8C43F21D84FA150B80D9083CB9BF6BEC663024_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.Byte[])
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeFrom_m496959C5DABEC8E4AFBD6277B4960F92581DCFAE (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m513DBFC93D76859C6B81B47EC5F546F6174716B2_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeFrom_m35222F82356FD2422BE24164EDC6125DF9D48154 (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m44A37F0234A9CDCAD8C4D09AC7F47932CD9BE893_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.IO.Stream)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeFrom_m64B17EDD1282A929E92A080231C411AEB35A6A6D (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_mCBB8D52A39B14EFC5B7ACFB43DDE748383500108_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeFrom_m1B4DB2FF2F658CEC5A805885FFCFA08D5D81CAC4 (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 *, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m81D9306326551B80945DAD0531EE64C07135FB0E_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeDelimitedFrom(System.IO.Stream)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeDelimitedFrom_mB19EE5F0FCF8140DD90EC96A03AD672683526FA6 (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m0505B5836359EC961E6065A397E671C33DA77353_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * AbstractBuilderLite_2_MergeDelimitedFrom_mD453C9051F68A00AE78E9EF83B8CF76CA5A95E06 (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * p0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * p1, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4 *, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 *, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m8022E9ECE595D0E6CCF5833FF1297912C17087D1_gshared)(__this, p0, p1, method);
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * OrientationEvent_CreateBuilder_m2EE04D707C94FC05F8365845F50D7FF61AB08011 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___prototype0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_mF49ADBA40C169B49ADB90170B1065D8DC3D4ED01 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent__ctor_mD121E47486ECAC2644B82A490759C5A601F98D9E (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_MakeReadOnly_mFB5CA6BBC67920FEA5DA878B0649863CF9EB46F2 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A (RuntimeArray * p0, RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  p1, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m7981F3DA5593744CAB82590CEB30C86C092DC9A4 (GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m47ECDA285A179A11564535C61736FEFDE7E55C8D_gshared)(__this, method);
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * GeneratedBuilderLite_2_MergeFrom_m776B1D0BC795128DB70DC82AA6E45A4D5508F8C7 (GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * (*) (GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m897D3373027F726F8270C9E4D6D5B08DAC83E648_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasTimestamp_m8D08CD65ED62AF90C119555524FC89A33877BEF6 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m600AA3CD27597FB8F3934D6706E284B05007B6C2 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasX_m99708562B7862EA08FD6F9EA6644DD0FA8AC8CC5 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_X(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_X_mBC2F2133A4DC87C22996B465CA9701EF7FACD00C (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasY_mAB26A61B246EF980AF74C362838FA7C231286EA1 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Y_mEB974C1FCFB88F2929624F1B79D7A01C1E7F3EC9 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasZ_m1504E50B9C7BF1727E54FBDBF2BFB4D1D6FEE315 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Z(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Z_m32DFBEFF42545455684D92E2F4E0ED3E95D33B4A (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasW()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasW_mDE97ABAD936A78CB7418E8F708CCB27804BAF427 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_W(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_W_m01E7F899F90E5ED175D5FA5696717C1D725DCEB8 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// Google.ProtocolBuffers.ExtensionRegistry Google.ProtocolBuffers.ExtensionRegistry::get_Empty()
extern "C" IL2CPP_METHOD_ATTR ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ExtensionRegistry_get_Empty_mC76700E7B42667A40F8433F271510D22D330BF29 (const RuntimeMethod* method);
// System.StringComparer System.StringComparer::get_Ordinal()
extern "C" IL2CPP_METHOD_ATTR StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * StringComparer_get_Ordinal_m1F38FBAB170DF80D33FE2A849D30FF2E314D9FDB (const RuntimeMethod* method);
// System.Int32 System.Array::BinarySearch<System.String>(!!0[],!!0,System.Collections.Generic.IComparer`1<!!0>)
inline int32_t Array_BinarySearch_TisString_t_mBE069358BEE81E8A0072F4E656453331D6AE5596 (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* p0, String_t* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	return ((  int32_t (*) (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, String_t*, RuntimeObject*, const RuntimeMethod*))Array_BinarySearch_TisRuntimeObject_mCA23887A81438487583626C2E739C0C9A3805B3E_gshared)(p0, p1, p2, method);
}
// Google.ProtocolBuffers.InvalidProtocolBufferException Google.ProtocolBuffers.InvalidProtocolBufferException::InvalidTag()
extern "C" IL2CPP_METHOD_ATTR InvalidProtocolBufferException_t33420AA524353DDC202DCBD0462457CC2AA7B4F9 * InvalidProtocolBufferException_InvalidTag_m9DBE12CAA1E271794334B86D0BA59CBD5648B1E5 (const RuntimeMethod* method);
// System.Boolean Google.ProtocolBuffers.WireFormat::IsEndGroupTag(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR bool WireFormat_IsEndGroupTag_mC38F0DEA5E885A3523C4B63DA3CC0DBB83F00B35 (uint32_t p0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetTimestamp_mB253470C04B3BDD48815ABF2F64834C4120F68AD (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, int64_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetX_mA979601CADE7564BA7A1CD07D58E8A90F28DC821 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetY_m92511A1A1498648A0545BFD9EF23B1CC6105A8AD (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetZ(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetZ_mBC47916BA958368D43AEBD2FB27547563F25AE78 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetW(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetW_m7CD765368D7839C39B4C6A4296AC8A4EAF1CB5C6 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/OrientationEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent__ctor_mD121E47486ECAC2644B82A490759C5A601F98D9E (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent__ctor_mD121E47486ECAC2644B82A490759C5A601F98D9E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_memoizedSerializedSize_18((-1));
		GeneratedMessageLite_2__ctor_m3900653DB821F4DDF90C2DA3A49DC84616BF3475(__this, /*hidden argument*/GeneratedMessageLite_2__ctor_m3900653DB821F4DDF90C2DA3A49DC84616BF3475_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = ((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->get_defaultInstance_0();
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_get_DefaultInstanceForType_m8925260733DE1C5DC508D453ABCA75E899F7EFEA (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_get_DefaultInstanceForType_m8925260733DE1C5DC508D453ABCA75E899F7EFEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91(/*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_ThisMessage()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_get_ThisMessage_m4E22AC5B7B1953D390F0876A1D477E11F2B04C78 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasTimestamp_m8D08CD65ED62AF90C119555524FC89A33877BEF6 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasTimestamp_4();
		return L_0;
	}
}
// System.Int64 proto.PhoneEvent/Types/OrientationEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t OrientationEvent_get_Timestamp_m27DD78A0CF1A72F2DC6F35D22915A9DC8CEBEAE2 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_timestamp__5();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasX_m99708562B7862EA08FD6F9EA6644DD0FA8AC8CC5 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasX_7();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_X()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_X_m9DC6D26DA8E3997CCCF3AAE2C3645E2EF572586D (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_x__8();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasY_mAB26A61B246EF980AF74C362838FA7C231286EA1 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasY_10();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Y()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Y_m6B75F5A2DC77CC5A2725BDFB6BBD3A50337C6E93 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_y__11();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasZ_m1504E50B9C7BF1727E54FBDBF2BFB4D1D6FEE315 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasZ_13();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Z()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Z_mFBA154D9C74251325CDB8A828C1A7FCC3292915C (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_z__14();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasW()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasW_mDE97ABAD936A78CB7418E8F708CCB27804BAF427 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasW_16();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_W()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_W_m61106CB54F7209E45F4761631647856D6ED39925 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_w__17();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_IsInitialized_m903165ABD3EEE452ADBED7F237EFEFB61C4D0C35 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::WriteTo(Google.ProtocolBuffers.ICodedOutputStream)
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent_WriteTo_m3DAC611BEBCD5363561CE7AE2DE615DE6D752738 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, RuntimeObject* ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_WriteTo_m3DAC611BEBCD5363561CE7AE2DE615DE6D752738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = ((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->get__orientationEventFieldNames_1();
		V_0 = L_0;
		bool L_1 = __this->get_hasTimestamp_4();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject* L_2 = ___output0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int64_t L_6 = OrientationEvent_get_Timestamp_m27DD78A0CF1A72F2DC6F35D22915A9DC8CEBEAE2(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< int32_t, String_t*, int64_t >::Invoke(1 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt64(System.Int32,System.String,System.Int64) */, ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89_il2cpp_TypeInfo_var, L_2, 1, L_5, L_6);
	}

IL_001e:
	{
		bool L_7 = __this->get_hasX_7();
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		RuntimeObject* L_8 = ___output0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 2;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		float L_12 = OrientationEvent_get_X_m9DC6D26DA8E3997CCCF3AAE2C3645E2EF572586D(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89_il2cpp_TypeInfo_var, L_8, 2, L_11, L_12);
	}

IL_0036:
	{
		bool L_13 = __this->get_hasY_10();
		if (!L_13)
		{
			goto IL_004e;
		}
	}
	{
		RuntimeObject* L_14 = ___output0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = 3;
		String_t* L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		float L_18 = OrientationEvent_get_Y_m6B75F5A2DC77CC5A2725BDFB6BBD3A50337C6E93(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89_il2cpp_TypeInfo_var, L_14, 3, L_17, L_18);
	}

IL_004e:
	{
		bool L_19 = __this->get_hasZ_13();
		if (!L_19)
		{
			goto IL_0066;
		}
	}
	{
		RuntimeObject* L_20 = ___output0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = 4;
		String_t* L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		float L_24 = OrientationEvent_get_Z_mFBA154D9C74251325CDB8A828C1A7FCC3292915C(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89_il2cpp_TypeInfo_var, L_20, 4, L_23, L_24);
	}

IL_0066:
	{
		bool L_25 = __this->get_hasW_16();
		if (!L_25)
		{
			goto IL_007e;
		}
	}
	{
		RuntimeObject* L_26 = ___output0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = 1;
		String_t* L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		float L_30 = OrientationEvent_get_W_m61106CB54F7209E45F4761631647856D6ED39925(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t36A5D9948E30CB6BE194EEDD1B24C3EE31689D89_il2cpp_TypeInfo_var, L_26, 5, L_29, L_30);
	}

IL_007e:
	{
		return;
	}
}
// System.Int32 proto.PhoneEvent/Types/OrientationEvent::get_SerializedSize()
extern "C" IL2CPP_METHOD_ATTR int32_t OrientationEvent_get_SerializedSize_mA55B8909849B20383F1D72807BF2688A23299D94 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_get_SerializedSize_mA55B8909849B20383F1D72807BF2688A23299D94_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_memoizedSerializedSize_18();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_000d:
	{
		V_0 = 0;
		bool L_3 = __this->get_hasTimestamp_4();
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		int64_t L_5 = OrientationEvent_get_Timestamp_m27DD78A0CF1A72F2DC6F35D22915A9DC8CEBEAE2(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t6FF414144C567C520A212C17587CA998689956E3_il2cpp_TypeInfo_var);
		int32_t L_6 = CodedOutputStream_ComputeInt64Size_m469FA3555088F5F6436873B0B862B27DAC90A566(1, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_6));
	}

IL_0026:
	{
		bool L_7 = __this->get_hasX_7();
		if (!L_7)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_8 = V_0;
		float L_9 = OrientationEvent_get_X_m9DC6D26DA8E3997CCCF3AAE2C3645E2EF572586D(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t6FF414144C567C520A212C17587CA998689956E3_il2cpp_TypeInfo_var);
		int32_t L_10 = CodedOutputStream_ComputeFloatSize_m590AFD0BAD36720168DBFB2EA91754E846F70BC7(2, L_9, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_10));
	}

IL_003d:
	{
		bool L_11 = __this->get_hasY_10();
		if (!L_11)
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_12 = V_0;
		float L_13 = OrientationEvent_get_Y_m6B75F5A2DC77CC5A2725BDFB6BBD3A50337C6E93(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t6FF414144C567C520A212C17587CA998689956E3_il2cpp_TypeInfo_var);
		int32_t L_14 = CodedOutputStream_ComputeFloatSize_m590AFD0BAD36720168DBFB2EA91754E846F70BC7(3, L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_14));
	}

IL_0054:
	{
		bool L_15 = __this->get_hasZ_13();
		if (!L_15)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_16 = V_0;
		float L_17 = OrientationEvent_get_Z_mFBA154D9C74251325CDB8A828C1A7FCC3292915C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t6FF414144C567C520A212C17587CA998689956E3_il2cpp_TypeInfo_var);
		int32_t L_18 = CodedOutputStream_ComputeFloatSize_m590AFD0BAD36720168DBFB2EA91754E846F70BC7(4, L_17, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_18));
	}

IL_006b:
	{
		bool L_19 = __this->get_hasW_16();
		if (!L_19)
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_20 = V_0;
		float L_21 = OrientationEvent_get_W_m61106CB54F7209E45F4761631647856D6ED39925(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t6FF414144C567C520A212C17587CA998689956E3_il2cpp_TypeInfo_var);
		int32_t L_22 = CodedOutputStream_ComputeFloatSize_m590AFD0BAD36720168DBFB2EA91754E846F70BC7(5, L_21, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)L_22));
	}

IL_0082:
	{
		int32_t L_23 = V_0;
		__this->set_memoizedSerializedSize_18(L_23);
		int32_t L_24 = V_0;
		return L_24;
	}
}
// System.Int32 proto.PhoneEvent/Types/OrientationEvent::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t OrientationEvent_GetHashCode_m732DB867B9C2526324D26CEA65C8DDDDBF68F3E7 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Type_t * L_0 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_hasTimestamp_4();
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_3 = V_0;
		int64_t* L_4 = __this->get_address_of_timestamp__5();
		int32_t L_5 = Int64_GetHashCode_mB5F9D4E16AFBD7C3932709B38AD8C8BF920CC0A4((int64_t*)L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
	}

IL_0022:
	{
		bool L_6 = __this->get_hasX_7();
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		float* L_8 = __this->get_address_of_x__8();
		int32_t L_9 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_0038:
	{
		bool L_10 = __this->get_hasY_10();
		if (!L_10)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_11 = V_0;
		float* L_12 = __this->get_address_of_y__11();
		int32_t L_13 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_12, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_11^(int32_t)L_13));
	}

IL_004e:
	{
		bool L_14 = __this->get_hasZ_13();
		if (!L_14)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_15 = V_0;
		float* L_16 = __this->get_address_of_z__14();
		int32_t L_17 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_15^(int32_t)L_17));
	}

IL_0064:
	{
		bool L_18 = __this->get_hasW_16();
		if (!L_18)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_19 = V_0;
		float* L_20 = __this->get_address_of_w__17();
		int32_t L_21 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_20, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_19^(int32_t)L_21));
	}

IL_007a:
	{
		int32_t L_22 = V_0;
		return L_22;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_Equals_m581EB99D43E5E5D2D7B9307DE40FF44B6E52E7B6 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_Equals_m581EB99D43E5E5D2D7B9307DE40FF44B6E52E7B6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)IsInstSealed((RuntimeObject*)L_0, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var));
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		bool L_2 = __this->get_hasTimestamp_4();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_hasTimestamp_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0035;
		}
	}
	{
		bool L_5 = __this->get_hasTimestamp_4();
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		int64_t* L_6 = __this->get_address_of_timestamp__5();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_7 = V_0;
		NullCheck(L_7);
		int64_t L_8 = L_7->get_timestamp__5();
		bool L_9 = Int64_Equals_mB589D15F558BF8FECBB56EF429EFF5C7A39D9E0F((int64_t*)L_6, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0037;
		}
	}

IL_0035:
	{
		return (bool)0;
	}

IL_0037:
	{
		bool L_10 = __this->get_hasX_7();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_hasX_7();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0060;
		}
	}
	{
		bool L_13 = __this->get_hasX_7();
		if (!L_13)
		{
			goto IL_0062;
		}
	}
	{
		float* L_14 = __this->get_address_of_x__8();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_15 = V_0;
		NullCheck(L_15);
		float L_16 = L_15->get_x__8();
		bool L_17 = Single_Equals_mCDFA927E712FBA83D076864E16C77E39A6E66FE7((float*)L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0062;
		}
	}

IL_0060:
	{
		return (bool)0;
	}

IL_0062:
	{
		bool L_18 = __this->get_hasY_10();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_hasY_10();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_20))))
		{
			goto IL_008b;
		}
	}
	{
		bool L_21 = __this->get_hasY_10();
		if (!L_21)
		{
			goto IL_008d;
		}
	}
	{
		float* L_22 = __this->get_address_of_y__11();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = L_23->get_y__11();
		bool L_25 = Single_Equals_mCDFA927E712FBA83D076864E16C77E39A6E66FE7((float*)L_22, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_008d;
		}
	}

IL_008b:
	{
		return (bool)0;
	}

IL_008d:
	{
		bool L_26 = __this->get_hasZ_13();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_27 = V_0;
		NullCheck(L_27);
		bool L_28 = L_27->get_hasZ_13();
		if ((!(((uint32_t)L_26) == ((uint32_t)L_28))))
		{
			goto IL_00b6;
		}
	}
	{
		bool L_29 = __this->get_hasZ_13();
		if (!L_29)
		{
			goto IL_00b8;
		}
	}
	{
		float* L_30 = __this->get_address_of_z__14();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_31 = V_0;
		NullCheck(L_31);
		float L_32 = L_31->get_z__14();
		bool L_33 = Single_Equals_mCDFA927E712FBA83D076864E16C77E39A6E66FE7((float*)L_30, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00b8;
		}
	}

IL_00b6:
	{
		return (bool)0;
	}

IL_00b8:
	{
		bool L_34 = __this->get_hasW_16();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_35 = V_0;
		NullCheck(L_35);
		bool L_36 = L_35->get_hasW_16();
		if ((!(((uint32_t)L_34) == ((uint32_t)L_36))))
		{
			goto IL_00e1;
		}
	}
	{
		bool L_37 = __this->get_hasW_16();
		if (!L_37)
		{
			goto IL_00e3;
		}
	}
	{
		float* L_38 = __this->get_address_of_w__17();
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_39 = V_0;
		NullCheck(L_39);
		float L_40 = L_39->get_w__17();
		bool L_41 = Single_Equals_mCDFA927E712FBA83D076864E16C77E39A6E66FE7((float*)L_38, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_00e3;
		}
	}

IL_00e1:
	{
		return (bool)0;
	}

IL_00e3:
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::PrintTo(System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent_PrintTo_m35DBBD6CEB205610733D810D83FAC3C370AE149F (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___writer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_PrintTo_m35DBBD6CEB205610733D810D83FAC3C370AE149F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_hasTimestamp_4();
		int64_t L_1 = __this->get_timestamp__5();
		int64_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var, &L_2);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_4 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3(_stringLiteralA5A01B8FA531FAAD566300A7EEDEFD11C4D01FDE, L_0, L_3, L_4, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3_RuntimeMethod_var);
		bool L_5 = __this->get_hasX_7();
		float L_6 = __this->get_x__8();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_7);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_9 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3(_stringLiteral11F6AD8EC52A2984ABAAFD7C3B516503785C2072, L_5, L_8, L_9, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3_RuntimeMethod_var);
		bool L_10 = __this->get_hasY_10();
		float L_11 = __this->get_y__11();
		float L_12 = L_11;
		RuntimeObject * L_13 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_12);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_14 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3(_stringLiteral95CB0BFD2977C761298D9624E4B4D4C72A39974A, L_10, L_13, L_14, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3_RuntimeMethod_var);
		bool L_15 = __this->get_hasZ_13();
		float L_16 = __this->get_z__14();
		float L_17 = L_16;
		RuntimeObject * L_18 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_17);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_19 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3(_stringLiteral395DF8F7C51F007019CB30201C49E884B46B92FA, L_15, L_18, L_19, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3_RuntimeMethod_var);
		bool L_20 = __this->get_hasW_16();
		float L_21 = __this->get_w__17();
		float L_22 = L_21;
		RuntimeObject * L_23 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_22);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_24 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3(_stringLiteralAFF024FE4AB0FECE4091DE044C58C9AE4233383A, L_20, L_23, L_24, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3557D0C2D7B666137AF55AF0B5B3B0AB3F286DD3_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_mA4CDBD35E004A50A76B87B7E10F766DA1B2EEB13 (ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_mA4CDBD35E004A50A76B87B7E10F766DA1B2EEB13_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * L_1 = ___data0;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = AbstractBuilderLite_2_MergeFrom_m51A99B2CC2EE65A2B8D33C7DFA55BBB9D3925F3F(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m51A99B2CC2EE65A2B8D33C7DFA55BBB9D3925F3F_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_mB405823EAFF5C1AFD6B6EF122898B280D5729991 (ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * ___data0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_mB405823EAFF5C1AFD6B6EF122898B280D5729991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		ByteString_t484699ED4C1CFF8928F9242C75BD5F6532CF768A * L_1 = ___data0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_3 = AbstractBuilderLite_2_MergeFrom_m9C44F027BC780DD743CB3B193540B3DF97CD068E(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m9C44F027BC780DD743CB3B193540B3DF97CD068E_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_mE97DBBD2F6C98DEC6DCB5DD5BDAB00B00A15F4A2 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_mE97DBBD2F6C98DEC6DCB5DD5BDAB00B00A15F4A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___data0;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = AbstractBuilderLite_2_MergeFrom_m496959C5DABEC8E4AFBD6277B4960F92581DCFAE(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m496959C5DABEC8E4AFBD6277B4960F92581DCFAE_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_m73A5952E28CB199961E941B40540B0AD69C3893D (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m73A5952E28CB199961E941B40540B0AD69C3893D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___data0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_3 = AbstractBuilderLite_2_MergeFrom_m35222F82356FD2422BE24164EDC6125DF9D48154(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m35222F82356FD2422BE24164EDC6125DF9D48154_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_mE020500B4CD835952E09898B82FDD034F74A4466 (Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_mE020500B4CD835952E09898B82FDD034F74A4466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = AbstractBuilderLite_2_MergeFrom_m64B17EDD1282A929E92A080231C411AEB35A6A6D(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m64B17EDD1282A929E92A080231C411AEB35A6A6D_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_m5C2ACFB25E390D22981AEC8F71776A156E93A1AD (Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m5C2ACFB25E390D22981AEC8F71776A156E93A1AD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = ___input0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_3 = AbstractBuilderLite_2_MergeFrom_m1B4DB2FF2F658CEC5A805885FFCFA08D5D81CAC4(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1B4DB2FF2F658CEC5A805885FFCFA08D5D81CAC4_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseDelimitedFrom_m67B8A477040C545D71769E23259BF1F2F9321D90 (Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseDelimitedFrom_m67B8A477040C545D71769E23259BF1F2F9321D90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = AbstractBuilderLite_2_MergeDelimitedFrom_mB19EE5F0FCF8140DD90EC96A03AD672683526FA6(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_mB19EE5F0FCF8140DD90EC96A03AD672683526FA6_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseDelimitedFrom_mB75C1304397BF83946189648904C7628122A3DCF (Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseDelimitedFrom_mB75C1304397BF83946189648904C7628122A3DCF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = ___input0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_3 = AbstractBuilderLite_2_MergeDelimitedFrom_mD453C9051F68A00AE78E9EF83B8CF76CA5A95E06(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_mD453C9051F68A00AE78E9EF83B8CF76CA5A95E06_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_mDCB8A835A3DC278361CEBCC50576707FEAB3FF0A (RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_mDCB8A835A3DC278361CEBCC50576707FEAB3FF0A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = VirtFuncInvoker1< Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *, RuntimeObject* >::Invoke(16 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, L_0, L_1);
		NullCheck(L_2);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_ParseFrom_m73086E4B29C6D7F7FB28C301E49F7D4299B84DED (RuntimeObject* ___input0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m73086E4B29C6D7F7FB28C301E49F7D4299B84DED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086(/*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_3 = VirtFuncInvoker2< Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *, RuntimeObject*, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, L_0, L_1, L_2);
		NullCheck(L_3);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m8EFF7C8079C6BCA53547AE439FA0E346A7C5CCF2_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * OrientationEvent_MakeReadOnly_mFB5CA6BBC67920FEA5DA878B0649863CF9EB46F2 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_CreateBuilder_mD547767979F88F2382A2960B92BD045B9BF21086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *)il2cpp_codegen_object_new(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49_il2cpp_TypeInfo_var);
		Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::ToBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * OrientationEvent_ToBuilder_mC9B7F27ABEC0AD40B243E77F34170D30B40A1A21 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ToBuilder_mC9B7F27ABEC0AD40B243E77F34170D30B40A1A21_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = OrientationEvent_CreateBuilder_m2EE04D707C94FC05F8365845F50D7FF61AB08011(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilderForType()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * OrientationEvent_CreateBuilderForType_m3313C216FC4A71AD4E03370190678EA46C6810CB (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_CreateBuilderForType_m3313C216FC4A71AD4E03370190678EA46C6810CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_0 = (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *)il2cpp_codegen_object_new(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49_il2cpp_TypeInfo_var);
		Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * OrientationEvent_CreateBuilder_m2EE04D707C94FC05F8365845F50D7FF61AB08011 (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___prototype0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_CreateBuilder_m2EE04D707C94FC05F8365845F50D7FF61AB08011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = ___prototype0;
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_1 = (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *)il2cpp_codegen_object_new(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49_il2cpp_TypeInfo_var);
		Builder__ctor_mF49ADBA40C169B49ADB90170B1065D8DC3D4ED01(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent__cctor_mAD299E8068E26BDA6F7A1691D05B02AC25859F9A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent__cctor_mAD299E8068E26BDA6F7A1691D05B02AC25859F9A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)il2cpp_codegen_object_new(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent__ctor_mD121E47486ECAC2644B82A490759C5A601F98D9E(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = OrientationEvent_MakeReadOnly_mFB5CA6BBC67920FEA5DA878B0649863CF9EB46F2(L_0, /*hidden argument*/NULL);
		((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->set_defaultInstance_0(L_1);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralA5A01B8FA531FAAD566300A7EEDEFD11C4D01FDE);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralA5A01B8FA531FAAD566300A7EEDEFD11C4D01FDE);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralAFF024FE4AB0FECE4091DE044C58C9AE4233383A);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralAFF024FE4AB0FECE4091DE044C58C9AE4233383A);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral11F6AD8EC52A2984ABAAFD7C3B516503785C2072);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral11F6AD8EC52A2984ABAAFD7C3B516503785C2072);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral95CB0BFD2977C761298D9624E4B4D4C72A39974A);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral95CB0BFD2977C761298D9624E4B4D4C72A39974A);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral395DF8F7C51F007019CB30201C49E884B46B92FA);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral395DF8F7C51F007019CB30201C49E884B46B92FA);
		((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->set__orientationEventFieldNames_1(L_7);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_8 = (UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB*)SZArrayNew(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB_il2cpp_TypeInfo_var, (uint32_t)5);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_9 = L_8;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_10 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A____FADC743710841EB901D5F6FBC97F555D4BD94310_5_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_9, L_10, /*hidden argument*/NULL);
		((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->set__orientationEventFieldTags_2(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_il2cpp_TypeInfo_var);
		RuntimeObject * L_11 = ((PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_il2cpp_TypeInfo_var))->get_Descriptor_0();
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_get_ThisBuilder_m8CA294FFCD4573300551D3426A119481C9E548C0 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m7981F3DA5593744CAB82590CEB30C86C092DC9A4(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m7981F3DA5593744CAB82590CEB30C86C092DC9A4_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91(/*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_mF49ADBA40C169B49ADB90170B1065D8DC3D4ED01 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_mF49ADBA40C169B49ADB90170B1065D8DC3D4ED01_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m7981F3DA5593744CAB82590CEB30C86C092DC9A4(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m7981F3DA5593744CAB82590CEB30C86C092DC9A4_RuntimeMethod_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		V_0 = L_1;
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_2 = (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)il2cpp_codegen_object_new(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent__ctor_mD121E47486ECAC2644B82A490759C5A601F98D9E(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = V_0;
		VirtFuncInvoker1< Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_0029:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_mCBE45C0B42C1ACA20D73AF1248C1FC73EC1C8A1D (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * Builder_get_MessageBeingBuilt_m067F6CE306C838B01B15A23C71B954BB407B8606 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_Clear_mAED01EBE4F83E902B41ECE689E2423C75ECC129B (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_mAED01EBE4F83E902B41ECE689E2423C75ECC129B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91(/*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_Clone_mE746997FB7380030F257D53226BA90972952B836 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_mE746997FB7380030F257D53226BA90972952B836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *)il2cpp_codegen_object_new(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49_il2cpp_TypeInfo_var);
		Builder__ctor_mF49ADBA40C169B49ADB90170B1065D8DC3D4ED01(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0014:
	{
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_3 = (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *)il2cpp_codegen_object_new(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49_il2cpp_TypeInfo_var);
		Builder__ctor_m7A923CA5C112F9695F5DFF9A8EE0500E42BB7ACA(L_3, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_5 = VirtFuncInvoker1< Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * Builder_get_DefaultInstanceForType_m8C95578C47726F0C0DBB1963A2EAE0781BC2E4B3 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m8C95578C47726F0C0DBB1963A2EAE0781BC2E4B3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91(/*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * Builder_BuildPartial_mFCCAB1AB850B69567E984EE5B6E64CB092F435B9 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_000f:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_3 = OrientationEvent_MakeReadOnly_mFB5CA6BBC67920FEA5DA878B0649863CF9EB46F2(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_MergeFrom_m56553FBB92C57C9D8281DE9E23A8BC24FF4C5A65 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m56553FBB92C57C9D8281DE9E23A8BC24FF4C5A65_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)IsInstSealed((RuntimeObject*)L_0, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = VirtFuncInvoker1< Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(!0) */, __this, ((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)CastclassSealed((RuntimeObject*)L_1, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0015:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m776B1D0BC795128DB70DC82AA6E45A4D5508F8C7(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m776B1D0BC795128DB70DC82AA6E45A4D5508F8C7_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_MergeFrom_mC1EB74CC5F94FDE8331D4FECC8C7E0297E9DD8BD (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_mC1EB74CC5F94FDE8331D4FECC8C7E0297E9DD8BD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = OrientationEvent_get_DefaultInstance_m17A4BC0CBC339EAABA2978C8CF353C38FF414D91(/*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)L_0) == ((RuntimeObject*)(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 *)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		return __this;
	}

IL_000a:
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = OrientationEvent_get_HasTimestamp_m8D08CD65ED62AF90C119555524FC89A33877BEF6(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_4 = ___other0;
		NullCheck(L_4);
		int64_t L_5 = OrientationEvent_get_Timestamp_m27DD78A0CF1A72F2DC6F35D22915A9DC8CEBEAE2(L_4, /*hidden argument*/NULL);
		Builder_set_Timestamp_m600AA3CD27597FB8F3934D6706E284B05007B6C2(__this, L_5, /*hidden argument*/NULL);
	}

IL_0025:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = OrientationEvent_get_HasX_m99708562B7862EA08FD6F9EA6644DD0FA8AC8CC5(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_8 = ___other0;
		NullCheck(L_8);
		float L_9 = OrientationEvent_get_X_m9DC6D26DA8E3997CCCF3AAE2C3645E2EF572586D(L_8, /*hidden argument*/NULL);
		Builder_set_X_mBC2F2133A4DC87C22996B465CA9701EF7FACD00C(__this, L_9, /*hidden argument*/NULL);
	}

IL_0039:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_10 = ___other0;
		NullCheck(L_10);
		bool L_11 = OrientationEvent_get_HasY_mAB26A61B246EF980AF74C362838FA7C231286EA1(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004d;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_12 = ___other0;
		NullCheck(L_12);
		float L_13 = OrientationEvent_get_Y_m6B75F5A2DC77CC5A2725BDFB6BBD3A50337C6E93(L_12, /*hidden argument*/NULL);
		Builder_set_Y_mEB974C1FCFB88F2929624F1B79D7A01C1E7F3EC9(__this, L_13, /*hidden argument*/NULL);
	}

IL_004d:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_14 = ___other0;
		NullCheck(L_14);
		bool L_15 = OrientationEvent_get_HasZ_m1504E50B9C7BF1727E54FBDBF2BFB4D1D6FEE315(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0061;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_16 = ___other0;
		NullCheck(L_16);
		float L_17 = OrientationEvent_get_Z_mFBA154D9C74251325CDB8A828C1A7FCC3292915C(L_16, /*hidden argument*/NULL);
		Builder_set_Z_m32DFBEFF42545455684D92E2F4E0ED3E95D33B4A(__this, L_17, /*hidden argument*/NULL);
	}

IL_0061:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_18 = ___other0;
		NullCheck(L_18);
		bool L_19 = OrientationEvent_get_HasW_mDE97ABAD936A78CB7418E8F708CCB27804BAF427(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0075;
		}
	}
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_20 = ___other0;
		NullCheck(L_20);
		float L_21 = OrientationEvent_get_W_m61106CB54F7209E45F4761631647856D6ED39925(L_20, /*hidden argument*/NULL);
		Builder_set_W_m01E7F899F90E5ED175D5FA5696717C1D725DCEB8(__this, L_21, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_MergeFrom_m276BB4322A07E03DE7E02DFD374A0C7FB2F85271 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m276BB4322A07E03DE7E02DFD374A0C7FB2F85271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_il2cpp_TypeInfo_var);
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_1 = ExtensionRegistry_get_Empty_mC76700E7B42667A40F8433F271510D22D330BF29(/*hidden argument*/NULL);
		Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * L_2 = VirtFuncInvoker2< Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 *, RuntimeObject*, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_MergeFrom_m74707E7D8855B78D106754E0AEAFFE0D6F86CC84 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, RuntimeObject* ___input0, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m74707E7D8855B78D106754E0AEAFFE0D6F86CC84_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = ((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->get__orientationEventFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE_il2cpp_TypeInfo_var);
		StringComparer_t588BC7FEF85D6E7425E0A8147A3D5A334F1F82DE * L_4 = StringComparer_get_Ordinal_m1F38FBAB170DF80D33FE2A849D30FF2E314D9FDB(/*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_mBE069358BEE81E8A0072F4E656453331D6AE5596(L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_mBE069358BEE81E8A0072F4E656453331D6AE5596_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_7 = ((OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_il2cpp_TypeInfo_var))->get__orientationEventFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_0041;
	}

IL_0031:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_011f;
	}

IL_0041:
	{
		uint32_t L_15 = V_0;
		if ((!(((uint32_t)L_15) <= ((uint32_t)((int32_t)21)))))
		{
			goto IL_0054;
		}
	}
	{
		uint32_t L_16 = V_0;
		if (!L_16)
		{
			goto IL_006b;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)8)))
		{
			goto IL_008b;
		}
	}
	{
		uint32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)21))))
		{
			goto IL_00a9;
		}
	}
	{
		goto IL_0071;
	}

IL_0054:
	{
		uint32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)29))))
		{
			goto IL_00c7;
		}
	}
	{
		uint32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)37))))
		{
			goto IL_00e5;
		}
	}
	{
		uint32_t L_21 = V_0;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)45))))
		{
			goto IL_0103;
		}
	}
	{
		goto IL_0071;
	}

IL_006b:
	{
		InvalidProtocolBufferException_t33420AA524353DDC202DCBD0462457CC2AA7B4F9 * L_22 = InvalidProtocolBufferException_InvalidTag_m9DBE12CAA1E271794334B86D0BA59CBD5648B1E5(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, NULL, Builder_MergeFrom_m74707E7D8855B78D106754E0AEAFFE0D6F86CC84_RuntimeMethod_var);
	}

IL_0071:
	{
		uint32_t L_23 = V_0;
		bool L_24 = WireFormat_IsEndGroupTag_mC38F0DEA5E885A3523C4B63DA3CC0DBB83F00B35(L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_007b;
		}
	}
	{
		return __this;
	}

IL_007b:
	{
		RuntimeObject* L_25 = ___input0;
		ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * L_26 = ___extensionRegistry1;
		uint32_t L_27 = V_0;
		String_t* L_28 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_25, L_26, L_27, L_28);
		goto IL_011f;
	}

IL_008b:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_29 = __this->get_result_1();
		RuntimeObject* L_30 = ___input0;
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_31 = __this->get_result_1();
		NullCheck(L_31);
		int64_t* L_32 = L_31->get_address_of_timestamp__5();
		NullCheck(L_30);
		bool L_33 = InterfaceFuncInvoker1< bool, int64_t* >::Invoke(2 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt64(System.Int64&) */, ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var, L_30, (int64_t*)L_32);
		NullCheck(L_29);
		L_29->set_hasTimestamp_4(L_33);
		goto IL_011f;
	}

IL_00a9:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_34 = __this->get_result_1();
		RuntimeObject* L_35 = ___input0;
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_36 = __this->get_result_1();
		NullCheck(L_36);
		float* L_37 = L_36->get_address_of_x__8();
		NullCheck(L_35);
		bool L_38 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var, L_35, (float*)L_37);
		NullCheck(L_34);
		L_34->set_hasX_7(L_38);
		goto IL_011f;
	}

IL_00c7:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_39 = __this->get_result_1();
		RuntimeObject* L_40 = ___input0;
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_41 = __this->get_result_1();
		NullCheck(L_41);
		float* L_42 = L_41->get_address_of_y__11();
		NullCheck(L_40);
		bool L_43 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var, L_40, (float*)L_42);
		NullCheck(L_39);
		L_39->set_hasY_10(L_43);
		goto IL_011f;
	}

IL_00e5:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_44 = __this->get_result_1();
		RuntimeObject* L_45 = ___input0;
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_46 = __this->get_result_1();
		NullCheck(L_46);
		float* L_47 = L_46->get_address_of_z__14();
		NullCheck(L_45);
		bool L_48 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var, L_45, (float*)L_47);
		NullCheck(L_44);
		L_44->set_hasZ_13(L_48);
		goto IL_011f;
	}

IL_0103:
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_49 = __this->get_result_1();
		RuntimeObject* L_50 = ___input0;
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_51 = __this->get_result_1();
		NullCheck(L_51);
		float* L_52 = L_51->get_address_of_w__17();
		NullCheck(L_50);
		bool L_53 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var, L_50, (float*)L_52);
		NullCheck(L_49);
		L_49->set_hasW_16(L_53);
	}

IL_011f:
	{
		RuntimeObject* L_54 = ___input0;
		NullCheck(L_54);
		bool L_55 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_tEF9FD579560DC089CE29D2ED0CB9EE39842DEDF5_il2cpp_TypeInfo_var, L_54, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_55)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasTimestamp_m9FFEB4C1764B3D2C054398972A9CEDF49A220C4D (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasTimestamp_4();
		return L_1;
	}
}
// System.Int64 proto.PhoneEvent/Types/OrientationEvent/Builder::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t Builder_get_Timestamp_m3478369E27C8B7E8F2EF50829D4CA3CF9C331B1D (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int64_t L_1 = OrientationEvent_get_Timestamp_m27DD78A0CF1A72F2DC6F35D22915A9DC8CEBEAE2(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m600AA3CD27597FB8F3934D6706E284B05007B6C2 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		Builder_SetTimestamp_mB253470C04B3BDD48815ABF2F64834C4120F68AD(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetTimestamp_mB253470C04B3BDD48815ABF2F64834C4120F68AD (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)1);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		int64_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_timestamp__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearTimestamp()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_ClearTimestamp_m2B7034E09C66FDEB7F356A04F97991D42B6EB766 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_timestamp__5((((int64_t)((int64_t)0))));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasX_mC1FAE33A4A3F2E7EF29BEAE67800D374B76B2F84 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasX_7();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_X()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_X_m1620E9CF048E7E1DD12CB27DBB7E29F58D25055E (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_X_m9DC6D26DA8E3997CCCF3AAE2C3645E2EF572586D(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_X(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_X_mBC2F2133A4DC87C22996B465CA9701EF7FACD00C (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetX_mA979601CADE7564BA7A1CD07D58E8A90F28DC821(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetX_mA979601CADE7564BA7A1CD07D58E8A90F28DC821 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasX_7((bool)1);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_x__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearX()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_ClearX_m650589B1A5614749EDF16CC8DBEDD668C9609E46 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasX_7((bool)0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_x__8((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasY_m6AF525AC04B9846AA4612A0FBBAB3F6E0992ECCC (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasY_10();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_Y()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_Y_m9EEDC6B795111FCD8314F547D7B04512CCFFFC7C (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_Y_m6B75F5A2DC77CC5A2725BDFB6BBD3A50337C6E93(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Y_mEB974C1FCFB88F2929624F1B79D7A01C1E7F3EC9 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetY_m92511A1A1498648A0545BFD9EF23B1CC6105A8AD(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetY_m92511A1A1498648A0545BFD9EF23B1CC6105A8AD (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasY_10((bool)1);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_y__11(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearY()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_ClearY_mE8331AB75374DB8779335024364406A1901F14B4 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasY_10((bool)0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_y__11((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasZ_mFACE4C7030C6FD38AF1506CF87ADB44BDDE84F8D (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasZ_13();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_Z()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_Z_mDD9CEBEC145590867B432570EEF874D6AE5713B9 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_Z_mFBA154D9C74251325CDB8A828C1A7FCC3292915C(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Z(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Z_m32DFBEFF42545455684D92E2F4E0ED3E95D33B4A (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetZ_mBC47916BA958368D43AEBD2FB27547563F25AE78(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetZ(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetZ_mBC47916BA958368D43AEBD2FB27547563F25AE78 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasZ_13((bool)1);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_z__14(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearZ()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_ClearZ_mEB6FD8C5F9D20C2D6760A458C7422FFEBF20FCE7 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasZ_13((bool)0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_z__14((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasW()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasW_m4A40E640FF651F7DD8A2048361DA7003EC5B0A99 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasW_16();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_W()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_W_m2ABCC1E8F0D6884F1838859049BF3ADCCDA03398 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_W_m61106CB54F7209E45F4761631647856D6ED39925(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_W(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_W_m01E7F899F90E5ED175D5FA5696717C1D725DCEB8 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetW_m7CD765368D7839C39B4C6A4296AC8A4EAF1CB5C6(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetW(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_SetW_m7CD765368D7839C39B4C6A4296AC8A4EAF1CB5C6 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasW_16((bool)1);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_w__17(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearW()
extern "C" IL2CPP_METHOD_ATTR Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * Builder_ClearW_mBEC70F114BB2DBE0C34A0BBEBBDEF7C66E61E180 (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_mB5251CEE65C1F67D0346EE7C6FECF5CFF3D06D0E(__this, /*hidden argument*/NULL);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasW_16((bool)0);
		OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_w__17((0.0f));
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.Proto.PhoneEvent::RegisterAllExtensions(Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR void PhoneEvent_RegisterAllExtensions_m61F8709869646B3E266AEAD2BD72BBA6A057B964 (ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___registry0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void proto.Proto.PhoneEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PhoneEvent__cctor_m43334645A72D0A443EDF366DB61D7C2A42CA69C1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhoneEvent__cctor_m43334645A72D0A443EDF366DB61D7C2A42CA69C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_il2cpp_TypeInfo_var))->set_Descriptor_0(NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

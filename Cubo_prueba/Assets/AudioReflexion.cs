﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioReflexion : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource source;
    public AudioClip texto_reflexion;
    public float empieza = 0.0f;
    public float reproduce = 0.0f;
    public bool flag;

    private void Awake()
    {

    }
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        source = GetComponent<AudioSource>();
        empieza += Time.deltaTime;
        if (empieza >= reproduce)
        {
            play();
        }

    }

    void play()
    {
        if (this.flag)
        {
            source.PlayOneShot(texto_reflexion);
        }
        this.flag = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start_Level : MonoBehaviour
{
    private AudioSource source;
    public AudioClip clip;
   

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        source.PlayOneShot(clip);

    }

    // Update is called once per frame
    void Update()
    {


    }

}

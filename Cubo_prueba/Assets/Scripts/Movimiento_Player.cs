﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_Player : MonoBehaviour
{

    public Transform playertf;
    Vector3 playervector;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        playertf.position += playervector;
    }

    public void moveXin(float x)
    {
        playervector = new Vector3(x, 0, 0);
    }
    public void moveZin(float z)
    {
        playervector = new Vector3(0, 0, z);
    }
    public void moveOut()
    {
        playervector = new Vector3(0, 0, 0);
    }
}

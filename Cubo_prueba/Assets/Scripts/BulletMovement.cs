﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed=20.0f;
    float elapsed;
    void Start()
    {
        elapsed = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        elapsed+= Time.fixedDeltaTime;
        transform.position += transform.forward * speed * Time.fixedDeltaTime;

        if (elapsed > 20)
        {
            GameObject.Destroy(gameObject);
        }
    }
}

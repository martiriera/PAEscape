﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buscaSalaRoja : MonoBehaviour
{
    public AudioClip smashSound;
    private AudioSource audioDiana;
    private bool audioDianaHasBeenPlayedOnce = false;
    public GameObject luzRoja;

    // Start is called before the first frame update
    void Awake()
    {
        audioDiana = GetComponent<AudioSource>();
        //luzRoja = GameObject.FindGameObjectWithTag("LuzRoja");
    }

   void OnCollisionEnter (Collision col)
   {
       Debug.Log("Is audio playing? " + InteractiveButton.audioStartIsPlaying);
       if(InteractiveButton.audioStartIsPlaying){
       Debug.Log("He entrado en el if");
       InteractiveButton.source.volume = 0.0f;
       //Show hidden arrows
       InteractiveButton.forwardArrow.SetActive(true);
       InteractiveButton.backguardsArrow.SetActive(true);
       InteractiveButton.leftArrow.SetActive(true);
       InteractiveButton.rightArrow.SetActive(true);
       //Destroy(GameObject.FindGameObjectWithTag("StartGameButton"));
       luzRoja.SetActive(true);
     }
      if(!audioDianaHasBeenPlayedOnce){
        audioDiana.PlayOneShot(smashSound, 1F);
        }

      Destroy(GameObject.FindGameObjectWithTag("bullet"));
      audioDianaHasBeenPlayedOnce = true;

   }
}

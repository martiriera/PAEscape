﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teclat : MonoBehaviour
{
    Renderer rend;
    public bool correcte;
    public Candau candau;
    public int numcol = 0;
    public float tiempo = 0.0f;
    public GameObject[] teclas = new GameObject[9];
    public int vidas = 3;
    public Animator doorLeft;
    public Animator doorRight;
    public Light light;
    private AudioSource source;
    public AudioClip sonidoCorrecto;
    public AudioClip sonidoIncorrecto;

   

// Start is called before the first frame update
void Start()
    {

        rend = GetComponent<Renderer>();
        teclas[0] = GameObject.Find("1(Clone)");
        teclas[1] = GameObject.Find("2(Clone)");
        teclas[2] = GameObject.Find("3(Clone)");
        teclas[3] = GameObject.Find("4(Clone)");
        teclas[4] = GameObject.Find("5(Clone)");
        teclas[5] = GameObject.Find("6(Clone)");
        teclas[6] = GameObject.Find("7(Clone)");
        teclas[7] = GameObject.Find("8(Clone)");
        teclas[8] = GameObject.Find("9(Clone)");

    }

    // Update is called once per frame
    void Update()
    {
        source = GetComponent<AudioSource>();
        tiempo += Time.deltaTime;    
      
    }

    private void Reset()
    {
        numcol = candau.UpdateCode(0, tiempo);
        for (int i=0; i < 9; i++)
        {
            teclas[i].GetComponent<Renderer>().material.color = Color.white;

        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "bullet")
        {
            Destroy(GameObject.FindGameObjectWithTag("bullet"));
            if (gameObject.tag == "2")
            {
                if (rend.material.color != Color.green)
                {

                    numcol = candau.UpdateCode(2, tiempo);
                   if(numcol == 2)
                    {
                        source.PlayOneShot(sonidoCorrecto);
                        rend.material.color = Color.green;
                    }
                    else
                    {
                        source.PlayOneShot(sonidoIncorrecto);
                        rend.material.color = Color.red;
                    }



                }
            }
            else if (gameObject.tag == "4")
            {
                if (rend.material.color != Color.green)
                {
                    numcol = candau.UpdateCode(4, tiempo);
                    if(numcol == 1)
                    {
                        source.PlayOneShot(sonidoCorrecto);
                        rend.material.color = Color.green;
                    }
                    else
                    {
                        source.PlayOneShot(sonidoIncorrecto);
                        rend.material.color = Color.red;
                    }



                }
            }
            else if (gameObject.tag == "7")
            {
                if (rend.material.color != Color.green)
                {

                    numcol = candau.UpdateCode(7, tiempo);
                    if(numcol == 3)
                    {
                        source.PlayOneShot(sonidoCorrecto);
                        rend.material.color = Color.green;
                    }
                    else
                    {
                        source.PlayOneShot(sonidoIncorrecto);
                        rend.material.color = Color.red;
                    }



                }
            }
            else
            {

                if(rend.material.color != Color.red)
                {
                    source.PlayOneShot(sonidoIncorrecto);
                    rend.material.color = Color.red;
                    numcol = candau.UpdateCode(3, tiempo);
                }


            }

            if (numcol == 3)
            {
                correcte = candau.Check();
                if (correcte == true)
                {
                    doorRight.SetBool("doorRight", false);
                    doorLeft.SetBool("doorLeft", false);
                    light.intensity = 1f;

                    for(int i = 0; i < 9; i++)
                    {
                        Destroy(teclas[i]);
                    }
                    //SceneManager.LoadScene("Menu");
                }
                else
                {

                    if (candau.Vida() > 0)
                    {
                        Reset();

                    }
                    else
                    {
                        SceneManager.LoadScene("Menu");
                    }
                }
            }



        }

     }
}

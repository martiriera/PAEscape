﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sala_Handler : MonoBehaviour
{

    public List<string> orden_salas = new List<string>(new string[] {"Juego0", "Juego1_1", "Juego1_2", "Juego1_3",
    "Juego1_4", "Juego2_1", "Juego2_2", "Juego2_3", "Juego2_4", 
    "Juego3_1", "Juego3_2", "Juego3_3", "Juego3_4", "Candado", "Menu"});
    public static string current_sala = "Juego0";


    public void change_sala(string current_sala)
    {
        current_sala = orden_salas[orden_salas.IndexOf(current_sala) + 1];
        SceneManager.LoadScene(current_sala);

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public Text txt;
    public static int scoreGeneral;
    public static int scoreSala1_1;
    public static int scoreSala1_2;
    public static int scoreSala1_3;
    public static int scoreSala1_4;
    public static int scoreSala2_1;
    public static int scoreSala2_2;
    public static int scoreSala2_3;
    public static int scoreSala2_4;
    public static int scoreSala3_1;
    public static int scoreSala3_2;
    public static int scoreSala3_3;
    public static int scoreSala3_4;
    public Sala_Handler sala_handler;



    /*private void Awake()
    {
        if(SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Escaperoom")){
            DontDestroyOnLoad(gameObject);
        }

    }*/
    //public int deltaScore;
    // Start is called before the first frame update
    void Start () {
        sala_handler = new Sala_Handler ();
    }

    // Update is called once per frame
    void Update () {
      Debug.Log("Global score = " + scoreGeneral);
        /*if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Escaperoom"))
        {
            DontDestroyOnLoad(gameObject);
        }*/
        txt.text = "Score:" + scoreGeneral;
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Menu")|| SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Menu1") || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Candado")) {
            txt.text = "";
        }

    }

    public void Destroy () {
        DestroyObject (gameObject);
    }

    public void ResetScore () {
        scoreGeneral = 0;
        scoreSala1_1 = 0;
        scoreSala1_2 = 0;
        scoreSala1_3 = 0;
        scoreSala1_4 = 0;
        scoreSala2_1 = 0;
        scoreSala2_2 = 0;
        scoreSala2_3 = 0;
        scoreSala2_4 = 0;
        scoreSala3_1 = 0;
        scoreSala3_2 = 0;
        scoreSala3_3 = 0;
        scoreSala3_4 = 0;

    }

    public void UpdateScore (int deltaScore) {
        scoreGeneral += deltaScore;
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego1_1")) {
            scoreSala1_1 += deltaScore;
            if (scoreSala1_1 == 5) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);
            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego1_2")) {
            scoreSala1_2 += deltaScore;
            if (scoreSala1_2 == 6) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego1_3")) {
            scoreSala1_3 += deltaScore;
            if (scoreSala1_3 == 8) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego1_4")) {
            scoreSala1_4 += deltaScore;

            if (scoreSala1_4 == 15) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);


            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego2_1")) {
            scoreSala2_1 += deltaScore;
            if (scoreSala2_1 == 4) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego2_2")) {
            scoreSala2_2 += deltaScore;
            if (scoreSala2_2 == 6) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego2_3")) {
            scoreSala2_3 += deltaScore;
            if (scoreSala2_3 == 8) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego2_4")) {
            scoreSala2_4 += deltaScore;
            if (scoreSala2_4 == 15) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego3_1")) {
            scoreSala3_1 += deltaScore;
            if (scoreSala3_1 == 4) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);

            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego3_2")) {
            scoreSala3_2 += deltaScore;
            if (scoreSala3_2 == 4) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);
            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego3_3")) {
            scoreSala3_3 += deltaScore;
            if (scoreSala3_3 == 12) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);
            }
        }
        if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Juego3_4")) {
            scoreSala3_4 += deltaScore;
            if (scoreSala3_4 == 13) {
                DestroyObject (gameObject);
                sala_handler.change_sala (SceneManager.GetActiveScene ().name);
            }
        }

    }
}

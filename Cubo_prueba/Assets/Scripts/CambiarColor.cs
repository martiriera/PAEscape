﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarColor : MonoBehaviour
{

    Renderer rend;
    public Score scoreScript;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            if (gameObject.tag == "letraerror")
            {
                if (rend.material.color != Color.green)
                {
                    rend.material.color = Color.green;
                    if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego1_1"))
                    {
                        scoreScript.UpdateScore(1);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego1_2"))
                    {
                        scoreScript.UpdateScore(3);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego1_3"))
                    {
                        scoreScript.UpdateScore(8);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego1_4"))
                    {
                        scoreScript.UpdateScore(5);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego2_1"))
                    {
                        scoreScript.UpdateScore(2);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego2_2"))
                    {
                        scoreScript.UpdateScore(2);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego2_3"))
                    {
                        scoreScript.UpdateScore(4);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego2_4"))
                    {
                        scoreScript.UpdateScore(5);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego3_1"))
                    {
                        scoreScript.UpdateScore(1);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego3_2"))
                    {
                        scoreScript.UpdateScore(1);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego3_3"))
                    {
                        scoreScript.UpdateScore(3);
                    }
                    else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Juego3_4"))
                    {
                        scoreScript.UpdateScore(13);
                    }

                }

            }
            else
            {
                rend.material.color = Color.red;
            }
            Destroy(GameObject.FindGameObjectWithTag("bullet"));
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Solletra : MonoBehaviour
{
    public AudioClip smashSound; 

    private AudioSource source; 
    // Start is called before the first frame update
    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
   void OnCollisionEnter (Collision col)
   {
       source.PlayOneShot(smashSound, 1F);
   }
}

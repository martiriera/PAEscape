using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveButton : MonoBehaviour {

  private float audioStartDelay = 0.0f;
  public float audioEndDelay = 0.0f;

  private float arrowStartDelay = 0.0f;
  private float arrowEndDelay = 17.2f;

  public static AudioSource source;
  public AudioClip smashSound;

  private bool startTimerFlag;
  public static  bool audioStartIsPlaying ;
  public static  bool audioStartHasBeenPlayed;
  public GameObject dianaTutorial;

  public static GameObject forwardArrow;
  public static GameObject backguardsArrow;
  public static GameObject leftArrow;
  public static GameObject rightArrow;

  public GameObject luzRoja;



  void Awake() {

      startTimerFlag = false;
      audioStartIsPlaying = false;
      audioStartHasBeenPlayed = false;

      source = GetComponent<AudioSource>();

      //luzRoja = GameObject.FindGameObjectWithTag("LuzRoja");
      luzRoja.SetActive(false);

      forwardArrow = GameObject.FindGameObjectWithTag("ForwardArrow");
      forwardArrow.SetActive(false);

      backguardsArrow = GameObject.FindGameObjectWithTag("BackguardsArrow");
      backguardsArrow.SetActive(false);

      leftArrow = GameObject.FindGameObjectWithTag("LeftArrow");
      leftArrow.SetActive(false);

      rightArrow = GameObject.FindGameObjectWithTag("RightArrow");
      rightArrow.SetActive(false);
  }

    // Start is called before the first frame update
    void Start()
    {
      dianaTutorial = GameObject.FindGameObjectWithTag("DianaTutorial");
      dianaTutorial.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
      enableDianaAfterPlayStartButtonAudio();
      hideMovingArrows();

      if(startTimerFlag && !audioStartIsPlaying){
        audioStartDelay += Time.deltaTime;
        if (audioStartDelay >= audioEndDelay) {
          source.PlayOneShot(smashSound, 1F);
          startTimerFlag = false;
          audioStartIsPlaying = true;
          audioStartHasBeenPlayed = true;
        }
      }
    }

    public void pointerEnter(){
      startTimerFlag = true;
    }

    public void enableDianaAfterPlayStartButtonAudio(){

      if(!InteractiveButton.audioStartHasBeenPlayed){
        //Debug.Log("Audio Start Has Been Played = " + InteractiveButton.audioStartHasBeenPlayed);
        dianaTutorial.SetActive(false);
      }

      else{
        //Debug.Log("Audio Start Has Been Played = " + InteractiveButton.audioStartHasBeenPlayed);
        dianaTutorial.SetActive(true);
      }

    }

    //Queremos que las flechas no se vean al inicio.
    public void hideMovingArrows(){
        if (audioStartHasBeenPlayed){
        arrowStartDelay += Time.deltaTime;
        if (arrowStartDelay >= arrowEndDelay) {
          forwardArrow.SetActive(true);
          backguardsArrow.SetActive(true);
          leftArrow.SetActive(true);
          rightArrow.SetActive(true);
        }
      }
    }
}

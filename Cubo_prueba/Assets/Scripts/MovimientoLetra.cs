﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoLetra : MonoBehaviour
{
    public GameObject caja;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //caja.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void RotarCubo1()
    {
        caja.transform.rotation = Quaternion.Euler(0, 0, 180);
    }

    public void InicioCubo1()
    {
        caja.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void RotarCubo2()
    {
        caja.transform.rotation = Quaternion.Euler(0, 90, 180);
    }

    public void InicioCubo2()
    {
        caja.transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    public void RotarCubo3()
    {
        caja.transform.rotation = Quaternion.Euler(0, -90, 180);
    }

    public void InicioCubo3()
    {
        caja.transform.rotation = Quaternion.Euler(0, -90, 0);
    }

    public void RotarCubo4()
    {
        caja.transform.rotation = Quaternion.Euler(0, 180, 180);
    }

    public void InicioCubo4()
    {
        caja.transform.rotation = Quaternion.Euler(0, 180, 0);
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarMensaje : MonoBehaviour
{
    //private AudioSource source;
    //public AudioClip sonidoreflexion;
    //public float tiempo_start=0.0f;
    //public float tiempo_reflexion = 0.0f;
    public Text miTexto;
    // Start is called before the first frame update

    void Awake()
    {
        //source = GetComponent<AudioSource>();
    }
    void Start()
    {
        //tiempo_start += Time.deltaTime;
        miTexto.text = "";
    }

    // Update is called once per frame
    void Update()
    { 

        miTexto.text = "                                                                Sala de Reflexión\n\n\n                                                                - Experimentar la dislexia\n                                                                - Dificultad en la lectura y la escritura\n                                                                - Mismas capacidades intelectuales\n                                                                - Puntuación: " + Score.scoreGeneral + "\n\n                                                                10% - 20% de la población la padece\n                                                                ¿Crees que estás bien informado\n                                                                acerca de la dislexia?";
    }

    /*void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            miTexto.text = "Sala de Reflexió\n\n\nEl Escape Room que acabas de hacer pretendía mostrar lo que experimenta una persona con dislexia. Aunque la experiencia ha sido muy visual, hay que tener en cuenta que la dislexia no es un problema de la vista, sino una dificultad en el aprendizaje de la lectura y la escritura que provoca que acaben tardando más a la hora de leer y escribir, pero acaban llegando al mismo lugar que el resto.\n\r\nComo has visto en la puntuación, parece que podrías tener dislexia. Esto es debido a los obstáculos que se te han presentado. Has tardado más de la cuenta en realizar las tareas que se te proponían, pero has podido escapar igualmente. Es decir, la dislexia no influye directamente en las capacidades intelectuales de la persona.\n\r\nEntre el 10 y el 20 por ciento de la población padece dislexia. A menudo, no se empatiza suficiente con esta afección y no somos conscientes de las dificultades que puede provocar en el día a día.\n\r\nY tú, ¿crees que empatizas con las personas que tienen dislexia?. Después de esta experiencia, ¿crees que en general, estamos bien informados acerca de la dislexia?\n\r\nEsperamos que te haya gustado la experiencia, que hagas un ejercicio de reflexión y, sobretodo, que te haya servido para entender mejor la dislexia.\r";

            //miTexto.text = "Sala de Reflexió\n\n\nL’objectiu d’aquest joc no era reproduïr-ne exactament els símptomes ja que no es tracta d’un problema visual. El que s’ha intentat és originar-te algunes de les sensacions i emocions que podria experimentar qui la pateix.\r\n\r\nCom pots veure a la barra, la teva puntuació ha estat de XX punts. Aquesta puntuació s’apropa a una persona dislèxica. Això és degut a què en aquesta simulació hem forçat que sentis en primera persona què experimenta una persona que pateix dislèxia. \r\nCom pots comprovar, l’únic que t’ha passat és que t’ha costat més temps processar les lletres i les paraules. \r\nAixò és el que pot sentir una persona que pateix aquesta disfunció en una situació en la vida real. Per exemple, en la vida real aquestes situacions es poden donar tant quan ets a l’escola, com en l’àmbit laboral.\r\n\r\nLa dislèxia és una disfunció que afecta de manera significativa en la lectura i escriptura del llenguatge, però tenen les mateixes capacitats intel·lectuals. La pateix un X% de la població.\r\n\r\nSovint, no s’empatitza suficient amb les persones dislèxiques per una manca d’informació i no som conscients de les dificultats que es troben en el seu dia a dia. \r\n\r\nI tu, quin grau de empatització creus que hi tens?\r\nDesprés d’aquesta simulació, creus que estaves prou informat?\r\n\r\nEsperem que t’hagi agradat la simulació i, sobretot, que t’hagi servit per entendre millor la DISLÈXIA.\r";
        }
    }
    */

}

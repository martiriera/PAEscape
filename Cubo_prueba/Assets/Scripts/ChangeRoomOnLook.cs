using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeRoomOnLook : MonoBehaviour {

    private float tiempoInicio = 0.0f;
    public float tiempoCambio = 0.0f;
    public float tiempoEmpiezaAudio = 0.0f;
    private bool startTimerFlag = false;
    private AudioSource source;
    public AudioClip smashSound;
    private bool so = true;

    // Start is called before the first frame update
    void Start()
    {
     source = GetComponent<AudioSource>();   
    }

    // Update is called once per frame
    void Update() {
        if(startTimerFlag) {
            tiempoInicio += Time.deltaTime;
            if (tiempoInicio >= tiempoEmpiezaAudio){
                if(so){
                sonido();
                } 
            }
            if (tiempoInicio >= tiempoCambio) {
                SceneManager.LoadScene("Menu");

            }
        }
        
    }

    public void pointerEnter(){
      startTimerFlag = true;
    }
    public void sonido(){
        source.PlayOneShot(smashSound, 1F);
        so = false;
    }

}
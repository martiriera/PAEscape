﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound_Number : MonoBehaviour
{
    public AudioSource number; 
    // Start is called before the first frame update
    void Start()
    {
        number = GetComponent<AudioSource>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter (Collision collision)
    {
        if (collision.gameObject.tag == "Target")
        {
            number.Play();
        }
    }
}

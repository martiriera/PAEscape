﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject prefab;
    public Transform cabeza;
    public AudioClip shootSound;

    private AudioSource source;
    private float volLowRange = .5f; 
    private float volHighRange = 1.0f; 

    void Awake()
    {
        source = GetComponent<AudioSource>(); 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(shootSound,vol);
            GameObject.Instantiate(prefab, cabeza.position, cabeza.rotation);
        }
    }
}

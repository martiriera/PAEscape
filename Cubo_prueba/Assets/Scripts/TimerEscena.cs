﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class TimerEscena : MonoBehaviour
{
    public float tiempo_start = 0.0f;
    public float tiempo_end = 0.0f;
    public float tiempo_recuerda = 0.0f;
    public Score score;
    public AudioClip sonidoRecuerda;
    private AudioSource source;
    private bool flag;
    private Sala_Handler sala_handler;

    // Start is called before the first frame update
    void Start()
    {
        sala_handler = new Sala_Handler();
    }

    void Awake()
    {
        source = GetComponent<AudioSource>();
        this.flag = true;
    }

    // Update is called once per frame
    void Update()
    {
        tiempo_start += Time.deltaTime;

        if (tiempo_start >= tiempo_end)
            sala_handler.change_sala(SceneManager.GetActiveScene().name);
    }

    public void reproduce()
    {
        if (this.flag)
        {
            source.PlayOneShot(sonidoRecuerda);
        }
        this.flag = false;

    }


}

﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideInfo : MonoBehaviour
{
    public GameObject info;
    private bool show = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowAndHideInfo()
    {
        if (!show)
        {
            info.SetActive(true);
            show = true;
        }
        else
        {
            info.SetActive(false);
            show = false;
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix1_4x4 : MonoBehaviour
{
    public Transform[] spawnLocations;
    public GameObject[] whatToSpawnPrefab;
    public GameObject[] whatToSpawnClone;

    void Start(){
        spawnSomethingPlease();
    }

    void spawnSomethingPlease(){
        whatToSpawnClone[0] = Instantiate(whatToSpawnPrefab[0],spawnLocations[0].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[1] = Instantiate(whatToSpawnPrefab[1],spawnLocations[1].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[2] = Instantiate(whatToSpawnPrefab[2],spawnLocations[2].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[3] = Instantiate(whatToSpawnPrefab[3],spawnLocations[3].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[4] = Instantiate(whatToSpawnPrefab[4],spawnLocations[4].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[5] = Instantiate(whatToSpawnPrefab[5],spawnLocations[5].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[6] = Instantiate(whatToSpawnPrefab[6],spawnLocations[6].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[7] = Instantiate(whatToSpawnPrefab[7],spawnLocations[7].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[8] = Instantiate(whatToSpawnPrefab[8],spawnLocations[8].transform.position,Quaternion.Euler(0,0,0)) as GameObject;
        whatToSpawnClone[9] = Instantiate(whatToSpawnPrefab[9], spawnLocations[9].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatToSpawnClone[10] = Instantiate(whatToSpawnPrefab[10], spawnLocations[10].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatToSpawnClone[11] = Instantiate(whatToSpawnPrefab[11], spawnLocations[11].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatToSpawnClone[12] = Instantiate(whatToSpawnPrefab[12], spawnLocations[12].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatToSpawnClone[13] = Instantiate(whatToSpawnPrefab[13], spawnLocations[13].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatToSpawnClone[14] = Instantiate(whatToSpawnPrefab[14], spawnLocations[14].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatToSpawnClone[15] = Instantiate(whatToSpawnPrefab[15], spawnLocations[15].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
    }

}

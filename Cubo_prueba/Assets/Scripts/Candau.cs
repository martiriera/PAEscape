﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candau : MonoBehaviour
{
    public int code;
    public int codi2, codi4, codi7;
    public float time2, time4, time7;
    private float tiempo = 0.0f; 
    public int numcol = 0;
    public int trash;
    public float ttrash;
    public int vida = 3;


    // Update is called once per frame
    void Update()
    {
        tiempo += Time.deltaTime;
    }
    
    public void ResetCode()
    {
        code = 0;
        codi2 = codi4 = codi7 = 0;
        time2 = time4 = time7 = 0;
    }

    public int UpdateCode (int deltaScore, float time)
    {
        if(deltaScore == 2)
        {
            codi2 = deltaScore;
            time2 = time;
            code += codi2;
            numcol++;
            return numcol;

        } else if (deltaScore == 4)
        {
            codi4 = deltaScore;
            time4 = time;
            code += codi4;
            numcol++;
            return numcol;

        } else if (deltaScore == 7)
        {
            codi7 = deltaScore;
            time7 = time;
            code += codi7;
            numcol++;
            return numcol;
        }
        else if(deltaScore == 0)
        {
            trash = deltaScore;
            ttrash = time;
            numcol = 0;
            code = 0;
            time2 = 0;
            time4 = 0;
            time7 = 0;
            codi2 = 0;
            codi4 = 0;
            codi7 = 0;

            return numcol;
        }
        else
        {
            trash = deltaScore;
            ttrash = time;
            numcol++;
            return numcol;
        }
        
    }

    public bool Check()
    {
        if (code == 13 && ((time4 < time2) & (time2 < time7)) ) 
        {
            return true;
        }
        else
            return false;               
       
    }
    public int Vida()
    {   
        
        if(vida > 0)
        {
            vida = vida - 1;
            return vida;
        }
        else
        {
            return 0;
        }        
    }
}
